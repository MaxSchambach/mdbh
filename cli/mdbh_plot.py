# -*- coding: utf-8 -*-1.3
# The MIT License (MIT)
# 
# Copyright © 2021 The MDBH Authors
# 
# You should have received a copy of the MIT License
# along with this program.  If not, see <https://mit-license.org/>.

"""A CLI to plot a Sacred metrics.

Either plots a single Sacred metric against a running index, or
plots two metrics against each other, one as the x- the other as the y-axis.

Provides smoothing via average or Gaussian filter and semilog and loglog plots.

"""

import argparse
from pathlib import Path

from mdbh import get_mongodb
from mdbh.plot import plot_metric


def parse_args(parser: argparse.ArgumentParser = None, parse=True):
    # Argument setup
    if parser is None:
        parser = argparse.ArgumentParser(
            description="Plot one or multiple metrics of "
                        "one or multiple sacred experiments.")

    parser.add_argument('-i', '--id', type=int,
                        required=True, nargs='+',
                        help='One or multiple MongoDB Sacred run IDs.')

    parser.add_argument('--idavg', action='store_true',
                        help='Whether to calculate averages and standard '
                             'deviations across multiple IDs.')

    parser.add_argument('-y', '--y', type=str,
                        required=True,
                        help='Name of the first metric to plot as y axis.')

    parser.add_argument('-x', '--x', type=str,
                        required=False,
                        help='Name of the second metric to plot as x axis. '
                             'If not set, uses integer indices.')

    parser.add_argument('-c', '--config', type=str,
                        default="~/.mongo.conf",
                        help='Path to MongoDB config file.')

    parser.add_argument('-d', '--db', type=str,
                        default="sacred",
                        help='Name of Sacred database.')

    filter_group = parser.add_mutually_exclusive_group()
    filter_group.add_argument('--avg', type=int,
                              help='Length of moving average filter to filter data. '
                                   'If 0, no smoothing is performed.')
    filter_group.add_argument('--sigma', type=float,
                              help='Sigma of the Gauss filter to filter data.')
    filter_group.add_argument('--beta', type=float,
                              help='Use exponential weighting with momentum beta.')

    option_group = parser.add_mutually_exclusive_group()
    option_group.add_argument('--semilogx', action='store_true',
                        help='Plot x axis logarithmic.')

    option_group.add_argument('--semilogy', action='store_true',
                        help='Plot y axis logarithmic.')

    option_group.add_argument('--loglog', action='store_true',
                        help='Plot x and y axis logarithmic.')
    if parse:
        return parser.parse_args()


def main(args):
    ids = args.id
    idavg = args.idavg
    label_x = args.x
    label_y = args.y
    config_path = Path(args.config).expanduser()
    db_name = args.db
    avg = args.avg
    sigma = args.sigma
    beta = args.beta
    semilogx = args.semilogx
    semilogy = args.semilogy
    loglog = args.loglog

    plot_norm = None
    if semilogx:
        plot_norm = "semilogx"
    elif semilogy:
        plot_norm = "semilogy"
    elif loglog:
        plot_norm = "loglog"

    average = None
    avg_arg = None
    if avg is not None:
        average = "ma"
        avg_arg = avg
    if sigma is not None:
        average = "gauss"
        avg_arg = sigma
    if beta is not None:
        average = "exp"
        avg_arg = beta

    db = get_mongodb(config_path, db_name)
    plot_metric(db=db, ids=ids,
                label_y=label_y, label_x=label_x,
                plot_norm=plot_norm,
                avg=average, avg_arg=avg_arg,
                id_avg=idavg, plt_show=True)

    return


if __name__ == '__main__':

    args = parse_args()
    main(args)
