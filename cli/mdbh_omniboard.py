# -*- coding: utf-8 -*-1.3
# The MIT License (MIT)
# 
# Copyright © 2021 The MDBH Authors
# 
# You should have received a copy of the MIT License
# along with this program.  If not, see <https://mit-license.org/>.

"""A convenience CLI to run the Omniboard server using the global
.mongo.conf file.

Simplifies setups with multi user, multi database and authentification.

"""

import argparse
import json
from pathlib import Path
import subprocess
from tempfile import NamedTemporaryFile
import os

from mdbh import get_uri, get_conf_databases


def parse_args(parser: argparse.ArgumentParser = None, parse=True):
    # Argument setup
    if parser is None:
        parser = argparse.ArgumentParser(
            description="Run the Omniboard server.")

    parser.add_argument('-c', '--config', type=str,
                        default="~/.mongo.conf",
                        help='Path to MongoDB config file.')

    parser.add_argument('-d', '--dbs', type=str, nargs='+',
                        help='Optional name of database. If not specified, '
                             'names are read from .mogno.conf file')

    parser.add_argument('--docker', action='store_true',
                        help='Start Omniboard within Docker container.')

    parser.add_argument('--sudo', action='store_true',
                        help='Whether to run command with sudo.')

    if parse:
        return parser.parse_args()


def main(args):
    config_path = Path(args.config).expanduser()
    dbs = args.dbs
    docker = args.docker
    sudo = args.sudo

    # Run commands with sudo?
    args = ['sudo'] if sudo else []

    with NamedTemporaryFile('w+', suffix='.json') as fh:

        dbs = dbs or get_conf_databases(config_path)

        omniboard_conf = dict()
        for i, db in enumerate(dbs):
            omniboard_conf[f'db{i}'] = dict()
            omniboard_conf[f'db{i}']['mongodbURI'] = get_uri(config_path, db)
            omniboard_conf[f'db{i}']['path'] = f'/db{i}'

        json.dump(obj=omniboard_conf, fp=fh)
        fh.flush()

        if docker:
            args += ['docker', 'run',
                     '--rm', '-p', '9000:9000',
                     '--mount', f'type=bind,source={fh.name},target=/.omniboard.json',
                     '-e', 'OMNIBOARD_CONFIG=/.omniboard.json',
                     '--name', 'omniboard', 'vivekratnavel/omniboard']
        else:
            os.environ['OMNIBOARD_CONFIG'] = fh.name
            args += ['omniboard']

        subprocess.run(args)


if __name__ == '__main__':
    args = parse_args()
    main(args)
