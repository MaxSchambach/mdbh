# -*- coding: utf-8 -*-1.3
# The MIT License (MIT)
# 
# Copyright © 2021 The MDBH Authors
# 
# You should have received a copy of the MIT License
# along with this program.  If not, see <https://mit-license.org/>.

try:
    from mdbh._version import __version__
except ImportError:
    __version__ = "local"

__author__ = "Maximilian Schambach"

from . import environ
from .core import get_conf_databases
from .core import get_mongodb
from .core import get_uri
from .sacred import get_artifact
from .sacred import get_artifact_names
from .sacred import get_dataframe
from .sacred import get_metric
from .sacred import get_run_names
from .sacred import resolve_artifacts

environ.set_defaults()
