# -*- coding: utf-8 -*-1.3
# The MIT License (MIT)
# 
# Copyright © 2021 The MDBH Authors
# 
# You should have received a copy of the MIT License
# along with this program.  If not, see <https://mit-license.org/>.


import os
import setuptools
from setuptools import setup
import subprocess

# Get Version from Pipeline, ignore leading 'v'
if os.environ.get('CI_COMMIT_TAG'):
    version = os.environ['CI_COMMIT_TAG'][1:]
elif os.environ.get('CI_JOB_ID'):
    version = os.environ['CI_JOB_ID']

# For local builds:
else:
    try:
        # Get latest git tag
        result = subprocess.run("git describe --tags", shell=True, stdout=subprocess.PIPE)
        version = result.stdout.decode('utf-8')[1:-1] + "-local"
    except:
        version = "local"

# Write version file for module to import
with open("mdbh/_version.py", "w") as f:
    f.write(f"__version__='{version}'")

# Load README as full description
short_description = "A MongoDB, Sacred, and Omniboard helper."
try:
    with open("README.md", "r") as f:
        long_description = f.read()
except:
    long_description = short_description

setup(
    python_requires='>=3.8',
    name='mdbh',
    version=version,
    description=short_description,
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/MaxSchambach/mdbh',
    author='Maximilian Schambach',
    author_email='schambach@kit.edu',
    install_requires=[
        'pymongo >= 3.0, <4.0',
        'pandas',
        'stopit >= 1.1.2',
    ],
    extras_require={
        "plot":  ["numpy", "matplotlib"],
    },
    license='MIT License',
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Development Status :: 4 - Beta",
    ],
    zip_safe=True)
